# -*- coding: utf-8 -*-
from __future__ import annotations

from snake.model.direction import Direction
from snake.model.position import Position
from snake.model.snake import Snake


def test_should_move_north() -> None:
    assert move(snake("(2, 2)"), Direction.North) == "(2, 1)"
    assert move(snake("(2, 2)|S"), Direction.North) == "(2, 1)|S"
    assert move(snake("(2, 2)|E"), Direction.North) == "(2, 1)|S"
    assert move(snake("(2, 2)|W"), Direction.North) == "(2, 1)|S"
    assert move(snake("(2, 2)|ESE"), Direction.North) == "(2, 1)|SES"


def move(snake: Snake, direction: Direction) -> str:
    return snake.move(direction).render()


def test_should_grow_north() -> None:
    assert grow(snake("(2, 2)"), Direction.North) == "(2, 1)|S"


def grow(snake: Snake, direction: Direction) -> str:
    return snake.grow(direction).render()


def test_should_contains_position() -> None:
    assert contains(snake("(2, 2)"), "(2, 2)")
    assert not contains(snake("(2, 2)"), "(1, 2)")
    assert contains(snake("(2, 2)|S"), "(2, 3)")
    assert contains(snake("(2, 2)|SS"), "(2, 4)")
    assert contains(snake("(2, 2)|SE"), "(3, 3)")


def contains(snake: Snake, position: str) -> bool:
    return Position.parse(position) in snake


def snake(value: str) -> Snake:
    match value.split("|"):
        case [head]:
            return Snake(head=Position.parse(head), tail=[])
        case [head, tail]:
            return Snake(head=Position.parse(head), tail=[Direction.parse(v) for v in tail])
    return Snake(head=head, tail=[])
