# -*- coding: utf-8 -*-
from __future__ import annotations


from snake.model.direction import Direction
from snake.model.position import Position


def test_should_move_north() -> None:
    assert move("(2, 2)", Direction.North) == "(2, 1)"


def test_should_move_south() -> None:
    assert move("(2, 2)", Direction.South) == "(2, 3)"


def test_should_move_east() -> None:
    assert move("(2, 2)", Direction.East) == "(3, 2)"


def test_should_move_west() -> None:
    assert move("(2, 2)", Direction.West) == "(1, 2)"


def test_opposite_move_should_do_nothing() -> None:
    double_move("(2, 2)", Direction.North)
    double_move("(2, 2)", Direction.South)
    double_move("(2, 2)", Direction.East)
    double_move("(2, 2)", Direction.West)


def double_move(position: str, direction: Direction) -> None:
    assert Position.parse(position).move(direction).move(direction.opposite()).render() == position


def move(position: str, direction: Direction) -> str:
    return Position.parse(position).move(direction).render()
