# -*- coding: utf-8 -*-
from __future__ import annotations

from snake.model.direction import Direction
from snake.model.position import Position
from snake.model.snake import Snake
from snake.model.world import World


def test_should_contains_position() -> None:
    assert contains(world(10, 5), "(2, 2)")
    assert contains(world(10, 5), "(2, 3)")
    assert contains(world(10, 5), "(2, 4)")
    assert contains(world(10, 5), "(3, 3)")
    assert not contains(world(10, 5), "(-1, 0)")
    assert not contains(world(10, 5), "(0, -1)")
    assert not contains(world(10, 5), "(11, 0)")
    assert not contains(world(10, 5), "(0, 6)")


def contains(world: World, position: str) -> bool:
    return Position.parse(position) in world


def world(width, height) -> World:
    return World.create(width, height, snake("(0, 0)"))


def snake(value: str) -> Snake:
    match value.split("|"):
        case [head]:
            return Snake(head=Position.parse(head), tail=[])
        case [head, tail]:
            return Snake(head=Position.parse(head), tail=[Direction.parse(v) for v in tail])
    return Snake(head=head, tail=[])
