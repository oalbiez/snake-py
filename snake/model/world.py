# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from enum import Enum, unique, auto

from snake.model.direction import Direction
from snake.model.position import Position
from snake.model.snake import Snake


@unique
class Status(Enum):
    Running = auto()
    Lost = auto()


@dataclass(frozen=True)
class World:
    status: Status
    width: int
    height: int
    snake: Snake

    @staticmethod
    def create(width: int, height: int, snake: Snake) -> World:
        return World(Status.Running, width, height, snake)

    def __contains__(self, position: Position) -> bool:
        return (0 <= position.x < self.width) and (0 <= position.y < self.height)
