# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass

from snake.model.direction import Direction
from snake.model.position import Position


@dataclass(frozen=True)
class Snake:
    head: Position
    tail: list[Direction]

    def move(self, direction: Direction) -> Snake:
        if self.tail:
            return Snake(
                head=self.head.move(direction),
                tail=[direction.opposite()] + self.tail[:-1])
        else:
            return Snake(
                head=self.head.move(direction),
                tail=self.tail)

    def grow(self, direction: Direction) -> Snake:
        return Snake(
            head=self.head.move(direction),
            tail=[direction.opposite()] + self.tail)

    def render(self) -> str:
        result = f"({self.head.x}, {self.head.y})"
        if self.tail:
            result += "|"
            result += "".join(d.render() for d in self.tail)
        return result

    def __contains__(self, position: Position) -> bool:
        if position == self.head:
            return True
        current = self.head
        for element in self.tail:
            current = current.move(element)
            if position == current:
                return True
        return False
