# -*- coding: utf-8 -*-
from __future__ import annotations

from enum import Enum, unique, auto

@unique
class Direction(Enum):
    North = auto()
    South = auto()
    East = auto()
    West = auto()

    def opposite(self) -> Direction:
        match self:
            case Direction.North:
                return Direction.South
            case Direction.South:
                return Direction.North
            case Direction.East:
                return Direction.West
            case Direction.West:
                return Direction.East

    @staticmethod
    def parse(value: str) -> Direction:
        match value:
            case "N":
                return Direction.North
            case "S":
                return Direction.South
            case "E":
                return Direction.East
            case "W":
                return Direction.West

    def render(self) -> str:
        match self:
            case Direction.North:
                return "N"
            case Direction.South:
                return "S"
            case Direction.East:
                return "E"
            case Direction.West:
                return "W"
