# -*- coding: utf-8 -*-
from __future__ import annotations

import re

from dataclasses import dataclass

from .direction import Direction

PATTERN = re.compile(r"\(\s*(?P<x>(\+|-)?\d+)\s*,\s*(?P<y>(\+|-)?\d+)\s*\)")


@dataclass(frozen=True)
class Offset:
    dx: int
    dy: int

    @staticmethod
    def parse(value: str) -> Offset:
        result = PATTERN.match(value)
        if result:
            return Offset(int(result.group('x')), int(result.group('y')))
        raise ValueError(f"'{value}' is not an Offset")

    @staticmethod
    def from_direction(direction: Direction) -> Offset:
        match direction:
            case Direction.North:
                return Offset(0, -1)
            case Direction.South:
                return Offset(0, 1)
            case Direction.East:
                return Offset(1, 0)
            case Direction.West:
                return Offset(-1, 0)


@dataclass(frozen=True)
class Position:
    x: int
    y: int

    @staticmethod
    def parse(value: str) -> Position:
        result = PATTERN.match(value)
        if result:
            return Position(int(result.group('x')), int(result.group('y')))
        raise ValueError(f"'{value}' is not a Position")

    def move(self, direction: Direction) -> Position:
        return self + Offset.from_direction(direction)

    def __add__(self, offset: Offset) -> Position:
        return Position(self.x + offset.dx, self.y + offset.dy)

    def render(self) -> str:
        return f"({self.x}, {self.y})"
