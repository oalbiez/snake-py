
SOURCES_FILES = $(shell find snake -type f -name '*.py')
TEST_FILES = $(shell find tests -type f -name '*.py')
SCRIPTS = scrap album
PYTHON_FILES = $(SOURCES_FILES) $(TEST_FILES) $(SCRIPTS)

format:
	@black --check .
	@isort --check --diff .

typecheck:
	@mypy \
		--install-types \
		--non-interactive \
		--show-error-codes \
		--linecount-report .report \
		snake
	@# TODO: tests checking is disabled because of error with mypy and numpy

lint:
	@pylint -j 4 $(PYTHON_FILES)

check: format typecheck lint

test:
	@pytest --hypothesis-profile=dev

debug-test:
	@pytest -vv --hypothesis-profile=dev

coverage:
	@pytest --hypothesis-profile=dev --cov=pyrecipe --cov-report=html:.htmlcov

reformat:
	@black $(PYTHON_FILES)
	@isort $(PYTHON_FILES)
